-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2016 at 09:48 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ondaba_app_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `notification_devices`
--

CREATE TABLE IF NOT EXISTS `notification_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `gcm_device_id` text,
  `is_active` tinyint(4) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`users_id`),
  KEY `fk_notification_devices_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `notification_devices`
--

INSERT INTO `notification_devices` (`id`, `users_id`, `gcm_device_id`, `is_active`, `created_on`) VALUES
(1, 3, 'jadf;kdagjahdjfnadkvalkf;jlasjdvajdvnalkdj;fkladhfiabdkvadmk;lvadjvandvadfjvlajf', 1, '2016-02-12 14:53:04'),
(2, 4, 'ldfadgoanfjaskdlfjadiosjklafjdsklafjashdglafjioawehfoajdiofagj', 1, '2016-02-12 14:53:04'),
(3, 5, 'lajdfladhgajlfadsjfghadsjfnhoawe90t8209o080ew9r2035i3poewjaodjfoa', 1, '2016-02-12 14:53:35'),
(4, 10, 'oiasldjfladshfauihfowjeoidjsfi457834042095u2oijojadofjoidfjoasfjoad;fjaoidjf', 1, '2016-02-12 14:53:35'),
(5, 11, 'adlkjagklajgl;jalkawo;adjfalhjdfljkfkaldladkjljfadjgoiwhfoahodjgasdkjfhdkfa;ldkfsa', 1, '2016-02-12 14:55:07'),
(6, 12, 'aldjfaljgaoifeadjslkldghaowoeihjoeht93240e9afjodijfoadjfawkjef;asdfnak;jdfnadkf', 1, '2016-02-12 14:55:07'),
(7, 13, 'a,dfjaljfkladjlfjweadhskfa;lekdfjalkdsfhioaheoawbgfald;smfbngklajsdofnaekhfnaldsklfad', 1, '2016-02-12 14:55:07'),
(8, 14, 'aldfjlahguaiefnhidahbvakjdfnuiasibfkabgiafk;asdfnkjdsgjkabdgkankasd;fkl,zcmnafj;oanjfa', 1, '2016-02-12 14:55:07'),
(9, 15, 'adlfja;ldfjalfak;jdn;kgn;afeiuhadgkadjfdfklgfgfakldc,mzm,vndakfa', 1, '2016-02-12 14:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `notification_messages`
--

CREATE TABLE IF NOT EXISTS `notification_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `account_id` varchar(100) DEFAULT NULL,
  `channel` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `notification_messages`
--

INSERT INTO `notification_messages` (`id`, `message`, `account_id`, `channel`) VALUES
(1, 'ladsjflkadsj faldskjf;la sfjskl', 'adslkfjlasdjlkgadsjl;fjads;jfkladsjfal', 'sms'),
(2, 'asdlkfjaklsjf asdfj;lasjfklasjf aslkfja', 'lasjdflajsdfladjklfasfldjsfal;', 'gcm'),
(3, 'kladsklfajdlfjasldjfasldjflkads flaskdjf las;fdsadfa', 'lkadsjflasjlfjasf', 'gmc'),
(4, 'lksajdfldjaflkajf asdklfj asfjl', 'sadlkfjadslg;asdjf asldjfl;asdjf a', 'sms');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(45) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `departure_date` date DEFAULT NULL,
  `destination` varchar(50) DEFAULT NULL,
  `min_number_of_people` int(11) DEFAULT NULL,
  `max_number_of_people` int(11) DEFAULT NULL,
  `unit_price` decimal(50,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `created_on`, `created_by`, `number_of_days`, `departure_date`, `destination`, `min_number_of_people`, `max_number_of_people`, `unit_price`) VALUES
(1, 'rwenzori hiking', '2016-02-12 00:00:00', 'elgon safaris', 3, '2016-02-13', 'rwenzori mountain', 6, 8, '200'),
(2, 'kilimanjaro suits', '2016-02-09 00:00:00', 'tuk tuk tours', 9, '2016-02-10', 'kilimanjaro mountain', 3, 5, '400'),
(3, 'source of the nile ', '2016-02-11 00:00:00', 'nile waters tours', 3, '2016-02-12', 'jinja', 3, 10, '150'),
(4, 'moroto terains', '2016-02-10 00:00:00', 'african tours', 5, '2016-02-12', 'karamoja', 3, 5, '300'),
(5, 'karuma view', '2016-02-12 00:00:00', 'tours of waters', 4, '2016-02-13', 'karuma falls', 3, 10, '250'),
(6, 'valley dump', '2016-02-12 14:03:50', 'kalahari tours', 6, NULL, 'lake turukana', 5, 8, '560'),
(7, 'elgon ski', '2016-02-12 14:05:26', 'kalahari tours', 6, NULL, 'mbale', 5, 8, '600'),
(8, 'elgon ski', '2016-02-12 14:05:26', 'kalahari tours', 6, NULL, 'mbale', 5, 8, '600'),
(9, 'everest ski', '2016-02-12 14:06:50', 'kalahari tours', 6, NULL, 'mbale', 5, 8, '5000');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `amount` decimal(45,0) DEFAULT NULL,
  `comment` text,
  `event_id` varchar(100) DEFAULT NULL,
  `payment_channel` tinyint(4) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payments_users_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `users_id`, `amount`, `comment`, `event_id`, `payment_channel`, `created_on`, `created_by`) VALUES
(1, 3, '500', 'oh can''t wait to experience the tour, come on take all the money', 'lasdflasjdlfjlgdasl', 1, '2016-02-13 00:00:00', 3),
(2, 5, '700', 'oh, this seems to be great, so i gatta pay all that i gat!', 'ladjfapofhwfoadnvahgioa', 2, '2016-02-12 14:29:10', 5),
(3, 3, '450', 'ldjalfjdklfj saldfj sd lorem ipsum dolo sit ameit', 'lsadjflajgdsa', 2, '2016-02-12 14:32:38', 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` tinyint(4) DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `last_udated` datetime DEFAULT CURRENT_TIMESTAMP,
  `role` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `email`, `first_name`, `middle_name`, `last_name`, `dob`, `marital_status`, `created_on`, `last_udated`, `role`) VALUES
(3, 'lajfajdfoawjelfjad;ljfaldg', 'lisa@yahoo.com', 'lisa', 'nabuka', 'shila', '2016-02-17', 1, '2016-02-11 00:00:00', '2016-02-02 00:00:00', 1),
(4, 'lkjafldjfajowjfpajf', 'masabatim@gmail.com', 'tom', 'masaba', 'billy', '2016-02-09', 2, '2016-02-18 00:00:00', '2016-02-18 00:00:00', 1),
(5, 'kljdfoapfhawefjklag', 'sharonniana@gmail.com', 'niana', 'sharon', 'adong', '2016-02-09', 1, '2016-02-17 00:00:00', '2016-02-26 00:00:00', 1),
(10, 'lajdflaldfjlalfnad;lfjd', 'hildakevin@gmail.com', 'hilda', 'kevin', 'akun', '1999-05-05', NULL, '2016-02-12 12:17:15', '2016-02-12 12:17:15', NULL),
(11, 'lajdflaldfjlalfnad;lfjd', 'hildakevin@gmail.com', 'hilda', 'kevin', 'akun', '1999-05-05', 2, '2016-02-12 12:18:22', '2016-02-12 12:18:22', 2),
(12, 'lajdflaldfjlalfnad;lfjd', 'hildakevin@gmail.com', 'hilda', 'kevin', 'akun', '1999-05-05', 2, '2016-02-12 12:19:56', '2016-02-12 12:19:56', 3),
(13, 'lajdflaldfjlalfnad;lfjd', 'cleverlykevin@gmail.com', 'cleverly', 'osmos', 'kevin', '1999-05-05', 1, '2016-02-12 12:27:35', '2016-02-12 12:27:35', 2),
(14, 'lajdflaldfjlalfnad;lfjd', 'cleverlykevin@gmail.com', 'cleverly', 'osmos', 'kevin', '1999-05-05', 1, '2016-02-12 12:29:09', '2016-02-12 12:29:09', 2),
(15, 'lajdfljljfdladdasdf;lfjd', 'agumamicheal@gmail.com', 'aguma', 'micheal', 'miky', '1999-05-05', 1, '2016-02-12 13:34:41', '2016-02-12 13:34:41', 2),
(16, 'lajdfljljfdladdasdf;lfjd', 'agumamicheal@gmail.com', 'lajflad', 'df;l', 'miky', '1999-05-05', 1, '2016-02-12 13:37:46', '2016-02-12 13:37:46', 2),
(17, 'lajdfddljljfdladdasdf;lfjd', 'ldkfjaldjgas@gmail.com', 'lajflad', 'df;l', 'yiuoiu', '1999-05-05', 1, '2016-02-12 13:38:40', '2016-02-12 13:38:40', 2),
(18, 'lajdfddljljfdladdasdf;lfjd', 'ldkfjaldjgas@gmail.com', 'pipoi', 'khgfd', 'szc', '1999-05-05', 1, '2016-02-12 13:39:31', '2016-02-12 13:39:31', 2),
(19, 'lajdfddljljfdladdasdf;lfjd', 'ldjlfjasf@gmail.com', 'pipoi', 'khgfd', 'gljsdgs', '1999-05-05', 1, '2016-02-12 13:40:08', '2016-02-12 13:40:08', 2),
(20, 'lajdfddljljfdladdasdf;lfjd', 'okemajefry@gmail.com', 'okema', 'jefrey', 'jildy', '1999-05-05', 1, '2016-02-12 13:45:22', '2016-02-12 13:45:22', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_packages`
--

CREATE TABLE IF NOT EXISTS `users_has_packages` (
  `users_id` int(11) NOT NULL,
  `packages_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`packages_id`),
  KEY `fk_users_has_packages_packages1_idx` (`packages_id`),
  KEY `fk_users_has_packages_users1_idx` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_has_packages`
--

INSERT INTO `users_has_packages` (`users_id`, `packages_id`) VALUES
(3, 1),
(4, 1),
(3, 2),
(11, 2),
(10, 5),
(11, 6);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `notification_devices`
--
ALTER TABLE `notification_devices`
  ADD CONSTRAINT `fk_notification_devices_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `fk_payments_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_packages`
--
ALTER TABLE `users_has_packages`
  ADD CONSTRAINT `fk_users_has_packages_packages1` FOREIGN KEY (`packages_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_packages_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
