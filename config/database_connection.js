var mysqlModel = require('mysql-model');
var Backbone = require('backbone');

// First you need to create a connection to the db
var db = mysqlModel.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "ondaba_app_db"
});

module.exports = db;