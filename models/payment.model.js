var db = require('../config/database_connection');

var Payment = db.extend({
    tableName: "payments"
});

module.exports = Payment;