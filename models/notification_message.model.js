var db = require('../config/database_connection');

var NotificationMessages = db.extend({
    tableName: "notification_messages"
});

module.exports = NotificationMessages;