var db = require('../config/database_connection');

var User = db.extend({
    tableName: "users"
});

module.exports = User;