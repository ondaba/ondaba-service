var db = require('../config/database_connection');

var NotificationDevices = db.extend({
    tableName: "notification_devices"
});

module.exports = NotificationDevices;