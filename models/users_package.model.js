var db = require('../config/database_connection');

var UserPackages = db.extend({
    tableName: "user_packages"
});

module.exports = UserPackages;