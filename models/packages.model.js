var db = require('../config/database_connection');

var Package = db.extend({
    tableName: "packages"
});

module.exports = Package;