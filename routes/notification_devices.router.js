var express = require('express');
var router = express.Router();
var NotificationDevicesController = require('../controllers/notification_devices.controller');


/* GET notification devices listing. */
router.get('/', function(req, res ) {
    NotificationDevicesController.index(req,res);
});

/* Get a notification device */
router.get('/:notDevId',function(req,res){
    NotificationDevicesController.show(req,res,req.params.notDevId);
});

/* Create a notification device */
router.post('/create',function(req,res){
    NotificationDevicesController.create(req,res);
});

/*Delete a notification device */
router.delete('/:notDevId/delete',function(req,res){
    NotificationDevicesController.deleteNotDevice(req,res,req.params.notDevId);
});

module.exports = router;
