var express = require('express');
var router = express.Router();
var UserController = require('../controllers/users.controller');





/* GET users listing. */
router.get('/', function(req, res,next) {
    UserController.index(req,res);
});

/* Get a user. */
router.get('/:userId',function(req,res,next){
    UserController.show(req,res,req.params.userId);
});



/* Create a user */
router.post('/create',function(req,res,next){
    UserController.create(req,res);
});

/* GET form to create new user */
router.put('/:userId/update', function(req, res,next) {
    UserController.update(req,res,req.params.userId);
});

/*Delete a user */
router.delete('/:userId/delete',function(req,res){
    UserController.deleteUser(req,res,req.params.userId);
});



module.exports = router;
