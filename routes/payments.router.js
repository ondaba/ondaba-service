var express = require('express');
var router = express.Router();
var PaymentsController = require('../controllers/payments.controller');


/* GET packages listing. */
router.get('/', function(req, res ) {
    PaymentsController.index(req,res);
});

/* Get a package */
router.get('/:paymentId',function(req,res){
    PaymentsController.show(req,res,req.params.paymentId);
});

/* Create a package */
router.post('/create',function(req,res){
    PaymentsController.create(req,res);
});

/*Delete a package */
router.delete('/:paymentId/delete',function(req,res){
    PaymentsController.deletePayment(req,res,req.params.paymentId);
});

module.exports = router;
