var express = require('express');
var router = express.Router();
var NotificationMessagesController = require('../controllers/notification_messages.controller');


/* GET notification messages listing. */
router.get('/', function(req, res ) {
    NotificationMessagesController.index(req,res);
});

/* Get a notification message */
router.get('/:notMsgId',function(req,res){
    NotificationMessagesController.show(req,res,req.params.notMsgId);
});

/* Create a notification message */
router.post('/create',function(req,res){
    NotificationMessagesController.create(req,res);
});

/*Delete a notification message */
router.delete('/:notMsgId/delete',function(req,res){
    NotificationMessagesController.deleteNotMessage(req,res,req.params.notMsgId);
});

module.exports = router;
