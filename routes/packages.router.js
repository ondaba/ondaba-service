var express = require('express');
var router = express.Router();
var PackagesController = require('../controllers/packages.controller');


/* GET packages listing. */
router.get('/', function(req, res ) {

    PackagesController.index(req,res);
});

router.get('/search',function(req,res){
    PackagesController.search(req,res);
});

/* Get a package */
router.get('/:packageId',function(req,res){
    PackagesController.show(req,res,req.params.packageId);
});

/* Create a package */
router.post('/create',function(req,res){
    PackagesController.create(req,res);
});

/* GET form to create new user */
router.put('/:packageId/update', function(req, res,next) {
    PackagesController.update(req,res,req.params.packageId);
});

/*Delete a package */
router.delete('/:packageId/delete',function(req,res){
    PackagesController.deletePackage(req,res,req.params.packageId);
});

module.exports = router;
