var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Odaba app web service' });
});

router.get('/index/new', function(req, res, next) {
    res.render('new_user');
});
module.exports = router;
