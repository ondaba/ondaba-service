var express = require('express');
var router = express.Router();
var UsersPackagesController = require('../controllers/users_packages.controller');


/* Get user packages */
router.get('/user/:userId/packages',function(req,res){
    UsersPackagesController.index(req,res,req.params.userId);
});

/* Get a user package */
router.get('/user/:userId/packages/:pkgId',function(req,res){
    UsersPackagesController.show(req,res,req.params.userId,req.params.pkgId);
});



module.exports = router;
