var model = require('../models/package.model.js');

var package = new model();

module.exports = {

    index: function (req, res) {
        package.find('all', function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });

    },
    search: function (req, res) {
        package.find('all',{where:"name LIKE '"+req.query.q+"%'"}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },

    show: function (req, res, packageId) {
        package.find('first', {where: 'id = ' + packageId}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },
    create: function (req, res) {
        var package = new model({
            name: req.body.name,
            created_by: req.body.created_by,
            number_of_days: req.body.number_of_days,
            destination: req.body.destination,
            departure_date: req.body.departure_date,
            min_number_of_people: req.body.min_number_of_people,
            max_number_of_people: req.body.max_number_of_people,
            unit_price: req.body.unit_price,
            expiry_date: req.body.expiry_date,
            image_url: req.body.image_url

        });


        res.send(package);

        package.save(function (err) {
            if (err) throw err;
        });


    },

    update: function (req, res, packageId) {

        package.read(packageId);

        package.set('name', req.body.name);
        package.set('created_by', req.body.created_by);
        package.set('number_of_days', req.body.number_of_days);
        package.set('destination', req.body.destination);
        package.set('departure_date', req.body.departure_date);
        package.set('min_number_of_people', req.body.min_number_of_people);
        package.set('max_number_of_people', req.body.max_number_of_people);
        package.set('unit_price', req.body.unit_price);
        package.set('expiry_date', req.body.expiry_date);
        package.set('image_url', req.body.image_url);


        //console.log(package);

        package.save();

        res.send("package updated");


    },

    deletePackage: function (req, res, packageId) {
        package.set('id', packageId);
        package.remove(function (err) {
            if (err) throw err;

            res.send('Package deleted successfully')
        });
    }

};