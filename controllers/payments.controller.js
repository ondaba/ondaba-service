var model = require('../models/payment.model.js');

var payment = new model();

module.exports = {

    index: function (req, res) {
        payment.find('all', function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });

    },

    show: function (req, res, paymentId) {
        payment.find('first', {where: 'id = ' + paymentId}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },
    create: function (req, res) {
        var payment = new model({
            users_id: req.body.users_id,
            amount: req.body.amount,
            comment: req.body.comment,
            event_id: req.body.event_id,
            payment_channel: req.body.payment_channel,
            created_by: req.body.created_by

        });


        res.send(payment);

        payment.save(function (err) {
            if (err) throw err;
        });


    },

    deletePayment: function (req, res, packageId) {
        payment.set('id', packageId);
        payment.remove(function (err) {
            if (err) throw err;

            res.send('Package deleted successfully')
        });
    }

};