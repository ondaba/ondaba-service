var model = require('../models/user.model.js');

var user = new model();

module.exports = {

    index: function (req, res) {
        user.find('all', function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });

    },

    show: function (req, res, userId) {
        user.find('first', {where: 'id = ' + userId}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },

    create: function (req, res) {
        var user = new model({
            account_id: req.body.account_id,
            email: req.body.email,
            first_name: req.body.first_name,
            middle_name: req.body.middle_name,
            last_name: req.body.last_name,
            password: req.body.password,
            dob: req.body.dob,
            marital_status: req.body.marital_status,
            role: req.body.role

        });

        console.log(req.body);
        console.log(req.body.aid + " " + req.body.email);


        res.send(user);

        user.save();


    },
    update: function (req, res, userId) {

        user.read(userId);

        user.set('account_id', req.body.account_id);
        user.set('email', req.body.email);
        user.set('first_name', req.body.first_name);
        user.set('middle_name', req.body.middle_name);
        user.set('last_name', req.body.last_name);
        user.set('password', req.body.password);
        user.set('dob', req.body.dob);
        user.set('marital_status', req.body.marital_status);
        user.set('role', req.body.role);


        //console.log(user);

        user.save();

        res.send("user updated");


    },

    deleteUser: function (req, res, userId) {
        user.set('id', userId);
        user.remove(function (err) {
            if (err) throw err;

            res.send('User deleted successfully')
        });
    }

};