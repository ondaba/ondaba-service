var model = require('../models/notification_message.model.js');

var notification_message = new model();

module.exports = {

    index:function(req,res){
        notification_message.find('all', function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });

    },

    show: function(req,res,notMsgId){
        notification_message.find('first',{where:'id = '+notMsgId}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },
    create:function(req,res){
        var notification_message = new model({
            message:req.body.message,
            account_id:req.body.account_id,
            channel:req.body.channel

        });



        res.send(notification_message);

        notification_message.save(function(err){
            if(err) throw err;
        });


    },

    deleteNotMessage: function(req,res,notMsgId){
        notification_message.set('id', notMsgId);
        notification_message.remove(function(err){
            if(err) throw err;

            res.send('Message deleted successfully')
        });
    }

};