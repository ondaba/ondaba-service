var model = require('../models/notification_device.model.js');

var notification_device = new model();

module.exports = {

    index:function(req,res){
        notification_device.find('all', function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });

    },

    show: function(req,res,notDevId){
        notification_device.find('first',{where:'id = '+notDevId}, function (err, rows) {
            if (err) throw err;

            res.send(rows);

        });
    },
    create:function(req,res){
        var notification_device = new model({
            users_id:req.body.users_id,
            gcm_device_id:req.body.gcm_device_id,
            is_active:req.body.is_active

        });



        res.send(notification_device);

        notification_device.save(function(err){
            if(err) throw err;
        });


    },

    deleteNotDevice: function(req,res,notDevId){
        notification_device.set('id', notDevId);
        notification_device.remove(function(err){
            if(err) throw err;

            res.send('Device deleted successfully')
        });
    }

};