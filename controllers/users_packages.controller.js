var model = require('../models/users_package.model.js');
var model1 = require('../models/package.model.js');

var user_package = new model();

var packages = new model1();

module.exports = {


    index: function (req, res, userId) {

        var query = "SELECT u.id as user_id, u.account_id, u.email," +
            " upkg.packages_id, p.name,p.created_on,p.created_by," +
            "p.number_of_days,p.departure_date,p.destination,p.min_number_of_people,p.max_number_of_people," +
            "p.unit_price FROM users as u " +
            "INNER JOIN user_packages as upkg ON (u.id = upkg.users_id) " +
            "INNER JOIN packages as p ON (upkg.packages_id = p.id) " +
            "WHERE u.id = " + userId;

        user_package.query(query, function (err, rows, fields) {
            res.send(rows);
        });

    },

    show: function (req, res, userId, pkgId) {
        var query = "SELECT u.id as user_id, u.account_id, u.email," +
            " upkg.packages_id, p.name,p.created_on,p.created_by," +
            "p.number_of_days,p.departure_date,p.destination,p.min_number_of_people,p.max_number_of_people," +
            "p.unit_price FROM users as u " +
            "INNER JOIN user_packages as upkg ON (u.id = upkg.users_id) " +
            "INNER JOIN packages as p ON (upkg.packages_id = p.id) " +
            "WHERE u.id = " + userId + "&& p.id = " + pkgId;

        user_package.query(query, function (err, rows, fields) {
            res.send(rows);
        });

    }

};